import UIKit
import CoreGraphics

public final class WingOTPView: UIView {
  
  enum Direction {
    case left
    case right
  }
  
  public enum RenderingMode {
    case `default`
    case verify
  }
  
  /// Get call when all OTP fields input and cursor is at the last textField.
  public var onComplete: ((String) -> Swift.Void)?
  
  /// Get call everytime when editing OTP
  public var onChange: ((Bool) -> Swift.Void)?
  
  // MARK: - Properties
  
  /// Current state of OTP. Return `true` if all UITextField has text, otherwise return `false`.
  public var canProceed: Bool {
    let hasEmpty: Bool = allTextFields.map { $0.text.orEmpty.isEmpty }.contains(true)
    return !hasEmpty
  }
  
  /// Return current pin code if available
  public var currentPINCode: String {
    let code = allTextFields.map { $0.text.orEmpty }
      .joined(separator: "")
      .trimmingCharacters(in: .whitespacesAndNewlines)
    return code
  }
  
  /// Border width for normal state. Default is 1.0
  public var borderWidth: CGFloat = 1.0
  
  /// Border color for normal state. Default is `UIColor.lightGray.withAlphaComponent(0.3)`
  public var borderColor: UIColor? = UIColor.lightGray.withAlphaComponent(0.3)
  
  /// Image to use when validation is success
  public var imageForSuccess: UIImage?
  
  /// Image to use when validation is success
  public var imageForFailure: UIImage? = nil
  
  /// Number of OTP code
  public var numberOfOTPCode: Int = 6
  
  /// Border color when become first responder. Default is .lightGray.
  public var onFocusBorderColor: UIColor? = UIColor.lightGray
  
  /// Border width when become first responder. Default is 1.5
  public var onFocusBorderWidth: CGFloat = 1.5
  
  /// Font for UITextField. Default is `UIFont.systemFont(ofSize: 24, weight: .semibold)`
  public var preferredFont: UIFont = UIFont.systemFont(ofSize: 24, weight: .semibold)
  
  /// Placeholder textColor for all UITextField.
  public var placeholderColor: UIColor = UIColor.white.withAlphaComponent(0.5)
  
  public var placeholderCharacter: String? = "0"
  
  public var isSecureTextEntry: Bool = false
  
  /// Enable or disable shadow. Dfeault is `false`.
  public var isShadowEnabled: Bool = false
  
  /// Color for shadow. Default is `UIColor.black.withAlphaComponent(0.15)`. Will ignore is isShadowEnabled is `true`.
  public var shadowColor: UIColor = UIColor.black.withAlphaComponent(0.15)
  
  /// Text color for all UITextField. Default is .white
  public var textColor: UIColor = .white
  
  /// Background color for all UITextField.
  public var textBackgroundColor: UIColor = UIColor(hexString: "A9CB37")
  
  public var lossFocusBackgroundColor: UIColor?
  
  public var spacing: CGFloat = 8.0
  
  // MARK: - Private properties
  
  private var allowEditing: Bool = true
  private var isAutoFillingFromSMS = false
  private var autoFillBuffer: [String] = []
  private var timeIntervalBetweenAutofilledCharactersFromSMS: Date?
  private var allTextFields: [WingOTPTextField] = []
  private var textFieldsIndexes: [WingOTPTextField: Int] = [:]
  private weak var currentTextField: WingOTPTextField? = nil
  private lazy var containerView: _CheckmarkView = {
    let view = _CheckmarkView()
    view.backgroundColor = backgroundColor
    return view
  }()
  
  // In case user delete backward, mark canProceed as false by default
  private var allowToProceed: Bool = true
  
  // MARK: - Init
  
  private let mode: RenderingMode
  
  public init(mode: RenderingMode = .default) {
    self.mode = mode
    super.init(frame: .zero)
  }
  
  public required init?(coder: NSCoder) {
    fatalError()
  }
  
  // MARK: - Actions
  
  public final func allowUserInput(_ enabled: Bool) {
    allowEditing = enabled
    
    switch enabled {
    case true:
      break
      
    case false:
      allTextFields.forEach {
        $0.setBorder(amount: borderWidth, borderColor: borderColor)
      }
    }
    allTextFields.first?.becomeFirstResponder()
  }
  
  public final func cleanUp() {
    allTextFields.forEach {
      $0.text?.removeAll()
      $0.backgroundColor = textBackgroundColor
    }
    allTextFields.first?.becomeFirstResponder()
  }
  
  public final func shouldBecomeFirstResponder() {
    allTextFields.forEach { $0.resignFirstResponder() }
    allTextFields.first?.becomeFirstResponder()
  }
  
  public final func applySuccessImageImmediately() {
    containerView.setImage(imageForSuccess)
  }
  
  public final func setBorderColor(_ color: UIColor) {
    allTextFields.forEach {
      $0.setBorder(amount: 1.0, borderColor: color)
    }
  }
  
  // MARK: - Layouts
  
  public final func prepareLayouts() {
    allTextFields = (0..<numberOfOTPCode).map { _ in buildOTPTextField() }
    
    for idx in 0 ..< allTextFields.count {
      textFieldsIndexes[allTextFields[idx]] = idx
    }
    
    let stackView = UIStackView(arrangedSubviews: allTextFields)
    stackView.translatesAutoresizingMaskIntoConstraints = false
    stackView.distribution = .fillEqually
    stackView.spacing = spacing
    addSubview(stackView)
    NSLayoutConstraint.activate([
      stackView.topAnchor.constraint(equalTo: topAnchor),
      stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
      stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
      stackView.trailingAnchor.constraint(equalTo: trailingAnchor)
    ])
    
    if mode == .verify {
      stackView.addArrangedSubview(containerView)
    }
  }
  
  public final func applyLossFocusBackgroundColor() {
    if let lossFocusBackgroundColor = lossFocusBackgroundColor {
      allTextFields.forEach { $0.backgroundColor = lossFocusBackgroundColor }
    }
  }
  
  private func buildOTPTextField() -> WingOTPTextField {
    let textField = WingOTPTextField()
    textField.onDelete = { [unowned self] textField in
      if let index = self.allTextFields.firstIndex(of: textField as! WingOTPTextField) {
        let backwardIndex = max(0, index - 1)
        self.allTextFields[backwardIndex].becomeFirstResponder()
      }
    }
    if #available(iOS 12.0, *) {
      textField.textContentType = .oneTimeCode
    }
    textField.font = preferredFont
    textField.attributedPlaceholder = isSecureTextEntry ? nil : NSAttributedString(string: placeholderCharacter.orEmpty, attributes: [.foregroundColor: placeholderColor])
    textField.translatesAutoresizingMaskIntoConstraints = false
    textField.delegate = self
    textField.textColor = textColor
    textField.borderStyle = .none
    textField.textAlignment = .center
    textField.roundCorners(amount: 8)
    textField.backgroundColor = textBackgroundColor
    textField.isSecureTextEntry = isSecureTextEntry
    textField.keyboardType = .numberPad
    textField.setBorder(amount: borderWidth, borderColor: borderColor)
    textField.widthAnchor.constraint(equalToConstant: NSObject.newWidth).isActive = numberOfOTPCode == 1
    let heightAnchor = textField.heightAnchor.constraint(equalTo: textField.widthAnchor, multiplier: 1.2)
    heightAnchor.priority = .defaultHigh
    heightAnchor.isActive = true
    textField.layer.shadowColor = shadowColor.cgColor
    textField.layer.shadowOffset = CGSize(width: 0, height: 0)
    textField.layer.shadowRadius = 2
    textField.layer.shadowOpacity = 1.0
    textField.layer.masksToBounds = !isShadowEnabled
    return textField
  }
}

// MARK: - UITextFieldDelegate

extension WingOTPView: UITextFieldDelegate {
  public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    // In case user delete backward, mark canProceed as false by default
    allowToProceed = !string.isEmpty
    
    let otpCount: Int = string.count
    ///We don't need any string that is more than the maximum allowed chatacters
    if otpCount > self.numberOfOTPCode {
      return false
    }
    
    ///We don't need any white space either
    if ((string == "" || string == " ") && range.length == 0) {
      
      ///But, auto-fill from SMS - before sending in the characters one by one - will
      ///send two empty strings ("") in succession very fast, unlike the speed a human may enter passcode.
      ///
      ///We need to check for it and have to decide/assume that what we have received is indeed auto-filled code from SMS.
      ///
      ///This has to be done since we use a new textfield for each character instead of a single text field with all characters.
      
      if string == "" {
        if let oldInterval = timeIntervalBetweenAutofilledCharactersFromSMS {
          if Date().timeIntervalSince(oldInterval) < 0.05 {
            self.isAutoFillingFromSMS = true
            timeIntervalBetweenAutofilledCharactersFromSMS = nil
          }
        }
        timeIntervalBetweenAutofilledCharactersFromSMS = Date()
      }
      return false
    }
    
    ///We check if the text is pasted.
    if otpCount > 1 {
      ///If the string is of the same length as the number of otp characters, then we proceed to
      ///fill all the text fields with the characters
      if otpCount == numberOfOTPCode {
        // Wrap in main, to fix issue where app language is not English.
        DispatchQueue.main.asyncAfter(deadline: .now()) { [self] in
          for (idx, element) in string.enumerated() {
            allTextFields[idx].text = String(element)
          }
        }
        
        textField.resignFirstResponder()
        self.touchesEnded(Set.init(arrayLiteral: UITouch()), with: nil)
        self.informDelegate(string)
      }
    } else {
      if isAutoFillingFromSMS {
        autoFillBuffer.append(string)
        
        ///`checkOtpFromMessagesCount` below specifically checks if the entered string is less than the maximum allowed characters.
        ///Since we are debouncing it, `checkOtpFromMessagesCount` will get called only once.
        ///And we don't allow any characters that are less than the allowed ones.
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(checkOtpFromMessagesCount), object: nil)
        self.perform(#selector(checkOtpFromMessagesCount), with: nil, afterDelay: 0.1)
        
        ///We check if the auto-fill from SMS has finished entering all the characters.
        ///In this case, we need only up to the maximum number of otp characters set by the developer.
        ///At a later stage, this might be controlled by a flag which will strictly allow only equal number of characters set by the `numberOfOTPCode` property.
        if autoFillBuffer.count == numberOfOTPCode {
          var finalOTP = ""
          for (idx, element) in autoFillBuffer.enumerated() {
            let otpChar = String(element)
            finalOTP += otpChar
            allTextFields[idx].text = otpChar
          }
          self.touchesEnded(Set.init(arrayLiteral: UITouch()), with: nil)
          self.informDelegate(finalOTP)
          isAutoFillingFromSMS = false
          autoFillBuffer.removeAll()
        }
        return false
      }
      
      switch allowEditing {
      case true:
        ///Normal text entry
        if range.length == 0 {
          textField.text = string
          setNextResponder(textFieldsIndexes[textField as! WingOTPTextField], direction: .right)
        } else if range.length == 1 {
          setNextResponder(textFieldsIndexes[textField as! WingOTPTextField], direction: string.isEmpty ? .left : .right)
          textField.text = string.isEmpty ? "" : string
        }
        
      case false:
        return false
      }
    }
    return false
  }
  
  public func textFieldDidBeginEditing(_ textField: UITextField) {
    textField.roundCorners(amount: 8)
    textField.setBorder(amount: allowEditing ? onFocusBorderWidth : borderWidth, borderColor: allowEditing ? onFocusBorderColor : borderColor)
    if let backgroundColor = lossFocusBackgroundColor, !textField.text.orEmpty.isEmpty {
      textField.backgroundColor = backgroundColor
    } else {
      textField.backgroundColor = textBackgroundColor
    }
    self.currentTextField = textField as? WingOTPTextField
  }
  
  public func textFieldDidEndEditing(_ textField: UITextField) {
    textField.setBorder(amount: borderWidth, borderColor: borderColor)
    if let backgroundColor = lossFocusBackgroundColor, !textField.text.orEmpty.isEmpty {
      textField.backgroundColor = backgroundColor
    } else {
      textField.backgroundColor = textBackgroundColor
    }
    self.currentTextField = nil
    switch allowToProceed {
    case true:
      onChange?(canProceed)
      
      // Update validation image
      if mode == .verify {
        switch canProceed {
        case true:
          containerView.setImage(imageForSuccess)
        case false:
          containerView.setImage(imageForFailure)
        }
      }
      
    case false:
      onChange?(false)
      
      // Update validation image
      if mode == .verify {
        containerView.setImage(imageForFailure)
      }
    }
  }
  
  private func setNextResponder(_ index: Int?, direction: Direction) {
    guard let index = index else { return }
    if direction == .left {
      index == 0 ? (self.resignFirstResponder(textField: allTextFields.first)) : (_ = allTextFields[(index - 1)].becomeFirstResponder())
      let textField = allTextFields[index]
      textField.backgroundColor = textBackgroundColor
    } else {
      index == numberOfOTPCode - 1 ? (self.resignFirstResponder(textField: allTextFields.last)) : (_ = allTextFields[(index + 1)].becomeFirstResponder())
    }
  }
  
  private func resignFirstResponder(textField: WingOTPTextField?) {
    textField?.resignFirstResponder()
    self.touchesEnded(Set.init(arrayLiteral: UITouch()), with: nil)
    var otpString = ""
    let numberOfEmptyTextFields: Int = allTextFields.reduce(0, { emptyTextsCount, textField in
      otpString += textField.text!
      return (textField.text ?? "").isEmpty ? emptyTextsCount + 1 : emptyTextsCount
    })
    
    if numberOfEmptyTextFields > 0 {
      return
    }
    self.informDelegate(otpString)
  }
  
  /// This method detects if the auto-filled code from SMS is less than that of the allowed number of characters.
  @objc
  private func checkOtpFromMessagesCount() {
    if autoFillBuffer.count < numberOfOTPCode {
      isAutoFillingFromSMS = false
      autoFillBuffer.removeAll()
    }
  }
  
  private func informDelegate(_ otp: String) {
    onComplete?(otp)
  }
}

fileprivate final class _CheckmarkView: UIView {
  
  private lazy var checkImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.backgroundColor = .clear
    imageView.contentMode = .scaleAspectFit
    imageView.tintColor = .systemGreen
    return imageView
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    addSubview(checkImageView)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    checkImageView.frame = bounds.insetBy(dx: 8, dy: 8).integral
  }
  
  final func setImage(_ image: UIImage?) {
    checkImageView.image = image
  }
}
