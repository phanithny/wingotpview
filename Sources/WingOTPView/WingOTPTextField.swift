//
//  WingOTPTextField.swift
//  
//
//  Created by Phanith on 2/11/21.
//

import UIKit

final class WingOTPTextField: UITextField {
  
  var onDelete: ((UITextField) -> Swift.Void)?
  
  override func caretRect(for position: UITextPosition) -> CGRect {
    return .init(origin: .init(x: self.bounds.midX, y: self.bounds.origin.y), size: .init(width: 0.1, height: 0.1))
  }
  
  override func closestPosition(to point: CGPoint) -> UITextPosition? {
    return self.endOfDocument
  }
  
  override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
    false
  }
  
  override func deleteBackward() {
    super.deleteBackward()
    
    onDelete?(self)
  }
}

