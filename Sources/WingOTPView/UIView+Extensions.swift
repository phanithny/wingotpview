//
//  UIView+Extensions.swift
//  
//
//  Created by Phanith on 2/11/21.
//

import UIKit

extension UIView {
  final func roundCorners(amount: CGFloat) {
    self.layer.masksToBounds = true
    self.clipsToBounds = true
    self.layer.cornerRadius = amount
    
    if #available(iOS 13.0, *) {
      self.layer.cornerCurve = .continuous
    }
  }
  
  final func setBorder(amount: CGFloat, borderColor: UIColor? = .clear) {
    self.layer.masksToBounds = true
    self.clipsToBounds = true
    self.layer.borderColor = borderColor?.cgColor
    self.layer.borderWidth = amount
  }
}

/**
 * Neat little extension to get the memory address of a variable, from this SO answer:
 * https://stackoverflow.com/a/41067053/5912335
 */
extension NSObject {
  static var deviceIsInLandscape: Bool {
    get { UIScreen.main.bounds.width > UIScreen.main.bounds.height }
  }
  
  static var deviceIsiPad: Bool {
    get { UIDevice.current.userInterfaceIdiom == .pad }
  }
  
  static var newWidth: CGFloat {
    get {
      if deviceIsiPad { return 400 }
      return deviceIsInLandscape ?
        UIScreen.main.bounds.height / 1.2 :
        UIScreen.main.bounds.width / 1.2
    }
  }
}

extension Optional where Wrapped == String {
  var orEmpty: String {
    switch self {
    case .some(let value):
      return value
      
    case .none:
      return ""
    }
  }
}

extension UIColor {
  convenience init(hexString: String) {
    let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    let scanner = Scanner(string: hexString)
    
    if (hexString.hasPrefix("#")) {
      scanner.scanLocation = 1
    }
    
    var color: UInt32 = 0
    scanner.scanHexInt32(&color)
    
    let mask = 0x000000FF
    let r = Int(color >> 16) & mask
    let g = Int(color >> 8) & mask
    let b = Int(color) & mask
    
    let red = CGFloat(r) / 255.0
    let green = CGFloat(g) / 255.0
    let blue = CGFloat(b) / 255.0
    
    self.init(red: red, green: green, blue: blue, alpha: 1)
  }
}
