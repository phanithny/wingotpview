import XCTest
@testable import WingOTPView

final class WingOTPViewTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(WingOTPView().text, "Hello, World!")
    }
}
