# Usage
```

  let otpView = WingOTPView()
    
    /// Get call when all OTP fields input and cursor is at the last textField.
    otpView.onComplete = { code in
      print(code)
    }
    
    /// Get call everytime when editing OTP.  Good place to bind with next button.
    otpView.onChange = { canProceed in
      print(canProceed)
    }
    
    /// Whether disable or enable user input.
    otpView.allowUserInput(true)
    
    /// Many other properties available
    otpView.numberOfOTPCode = 6
    otpView.onFocusBorderWidth = 0.0
    otpView.textColor = .white
    
    otpView.layout {
      view.addSubview($0)
      $0.center()
        .height(50)
    }
    
    // Call to render layout
    otpView.prepareLayouts()
    
```
